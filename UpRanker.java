import org.javacord.api.DiscordApi;
import org.javacord.api.DiscordApiBuilder;
import org.javacord.api.entity.server.*;
import org.javacord.api.entity.server.invite.*;
import org.javacord.api.entity.channel.*;
import org.javacord.api.listener.*;
import org.javacord.api.entity.user.*;
import org.javacord.api.entity.permission.*;

import java.io.*;
import java.util.Collection;
import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.time.Instant;
import java.time.*;

public class UpRanker{
    public static void main(String[] args) {
        BotRunner bot;
        try {
            bot=new BotRunner();
            bot.run();
        } catch(BotException e) {
            System.out.println("Exception caught. Reason: [" + e.getExceptionCause() + "]");
        }
        return;
    }
}

    class BotException extends Exception {
        String exceptionCause;
        BotException(String cause) {
            exceptionCause = cause;
        }
        String getExceptionCause() {
            return exceptionCause;
        }

    }

// this miiiiight be getting a tad too long
class BotRunner{

    // api object
    DiscordApi api;

    // target objects
    Server serverAegis;
    Role memberRole;

    // data feed channel
    TextChannel feedChannel;

    // id numbers for finding target server and role
    long serverAegisId = 754380895221448845L;
    // long serverAegisId = 558390452148109357L;
    long memberRoleId = 754383243217993840L;
    // long memberRoleId = 915194562971041812L;

    // userid we compare new member's id to
    long userId = 822535414203023430L;

    // feed channel id
    long feedId = 922873483602563203L;
    // long feedId = 921911877632688178L;

    // current invite links
    Collection<RichInvite> invites;

    // thread handle
    TasksThread tasksThread;

    BotRunner() throws BotException {
        
        // extracting bot token from './data/token' file
        String token = getToken();
        if(token.equals(""))
            throw new BotException("Error while reading token.");

        // initializing api and connecting
        api = new DiscordApiBuilder().setToken(token).setAllIntents().login().join();

        // find target server
        findServer();

        // find feed channel where bot will post server information
        findFeedChannel();

        // find target role we assign to new member
        findRole();

        // create new thread
        tasksThread = new TasksThread();


        // here through lambda expression we create a nameless class that implements PeriodicTask interface (defined later)
        // basically a function that will be run periodically or when thread *tasksThread* is interrupted
        // this task can download new invite links and find time until next invite link expires
        // then this value is taken into consideration for how long thread is supposed to sleep
        // possibly a dumb way to do it but sounded fun to implement
        tasksThread.registerTask((r) -> {
                // default longest time until awake, if we don't find anything shorter - return this
                long nextAwakeRet = 60L*60L*24L*14L;

                // flags the tasks recognizes
                if( r == InterruptReason.UPDATE_SLEEP_TIME_INVITES_REFRESHER ||
                    r == InterruptReason.NONE ||
                    r == InterruptReason.PULL_NEW_SERVER_INVITES) {
                    
                    // download new invite links
                    if(r == InterruptReason.NONE || r == InterruptReason.PULL_NEW_SERVER_INVITES){
                        try {
                            getExistingInvites().get();     // this can throw exceptions so we need try block
                        } catch (InterruptedException e) {
                            return 1L;                      // will try again in 1 second if error occurs
                        } catch (ExecutionException e) { 
                            return 1L;                       
                        }
                    }

                    // find next awake time
                    for(RichInvite inv : invites) {
                        long invMaxDuration = (long) inv.getMaxAgeInSeconds();
                        if(invMaxDuration == 0)         // infinite invite link duration, ignore
                            continue;
                        // creation date + invite duration - current time = time until awake
                        long inviteExp = inv.getCreationTimestamp().getEpochSecond() + invMaxDuration - Instant.now().getEpochSecond();
                        
                        if(inviteExp < nextAwakeRet)    // new soonest awake time
                            nextAwakeRet = inviteExp;
                    }
                    return nextAwakeRet;
                } else {                                // flag not regecognized
                    return -1L;
                }
            }, 60L*60L*24L*14L);

        // starts execution of second threads
        tasksThread.start();


        // hook executed when bot stops, not needed right now but why not
        addShutdownHook();


    }
    
    // method adds listeners for events we look for
    void run() { 

        api.addServerMemberJoinListener(event -> {
            // cache old invite links
            Collection<RichInvite> cachedInvites = invites;
            // get new invite links
            try {
                getExistingInvites().get();
            } catch (InterruptedException e) {
                return;
            } catch (ExecutionException e) {
                return;
            }
            
            // search for differences between old and new links
            ArrayList<RichInvite> diffs = findInvLinksDifference(cachedInvites);
            // add differences to the string we later post in feed channel
            String codesList = " ";
            for(RichInvite inv : diffs)
                codesList += inv.getCode() + " ";

            
            User newUser = event.getUser();
            long uId = newUser.getId();

            // assigning a role to our quitter
            if(uId == userId) {
                newUser.addRole(memberRole);
            }

            // send message to the feed channel
            feedChannel.sendMessage("User [ " + newUser.getDiscriminatedName() + " ] has joined with invite link: [" + codesList + "]");
        });

        api.addServerMemberLeaveListener(event -> {
            feedChannel.sendMessage("[ " + event.getUser().getDiscriminatedName() + " ] has left the server.");
        });

        api.addServerChannelInviteCreateListener(event -> {
            Invite inv = event.getInvite();
            String invitee = inv.getInviter().get().getDiscriminatedName();
            feedChannel.sendMessage("Invite [ " + inv.getCode() + " ] has been created by: " + invitee + ".");
            tasksThread.reason = InterruptReason.PULL_NEW_SERVER_INVITES;
            tasksThread.interrupt();
        });

        api.addServerChannelInviteDeleteListener(event -> {
            tasksThread.reason = InterruptReason.PULL_NEW_SERVER_INVITES;
            tasksThread.interrupt();
        });

    }    

    // private methods

    // reading token from file, code is on gitlab so better not show it publicly lol
    private String getToken(){
        try{
            File token=new File("data/token");

            BufferedReader br=new BufferedReader(new FileReader(token));
            String ret=br.readLine();
            
            br.close();
            return ret;
        }catch(Exception e){
            e.printStackTrace();
            return "";
        }
    }

    private void findServer() throws BotException {

        System.out.println("Searching for target server...");

        Collection<Server> servers = api.getServers();
        if(servers.size() == 0) {
            // no servers found - abort
            api.disconnect();
            throw new BotException("No available servers found.");
        }

        // loop checks every server if its id matches target server
        for(Server s : servers) {
            System.out.println("Looking at: " + s.getName() + " id:[" + s.getId() + "]");
            if(s.getId() == serverAegisId) {
                // found target server, breaking out of the loop
                System.out.println("Found target server.");
                serverAegis = s;
                break;
            }
        }

        if(serverAegis == null) {
            // abort
            api.disconnect();
            throw new BotException("Cannot find Aegis discord.");
        }

    }

    private void findRole() throws BotException {

        System.out.println("Searching for target role...");

        // get all roles from the given server
        List<Role> serverRoles = serverAegis.getRoles();
        if(serverRoles.size() == 0) {
            // no roles found - abort
            api.disconnect();
            throw new BotException("No servers found.");
        } else {
            // loop checks every role if its id matches target role
            for(Role r : serverRoles) {
                System.out.println("Looking at: " + r.getName() + " id:[" + r.getId() + "]");

                if( r.getId() == memberRoleId) {
                    // found the role, breaking out of the loop
                    memberRole = r;
                    System.out.println("Found target role.");
                    break;
                }
            }
        }

        if(memberRole == null) {
            // no fitting role found - abort
            api.disconnect();
            throw new BotException("Cannot find target role.");
        }

    }

    private void findFeedChannel() throws BotException {
        List<ServerTextChannel> textChannels = serverAegis.getTextChannels();
        if(textChannels.size() == 0) {
            // target channel not found - abort
            api.disconnect();
            throw new BotException("Target feed channel not found.");
        } else {
            // loop through channels pulled from the target server and check if its id matches target channel
            for(ServerTextChannel channel : textChannels) {
                if(channel.getId() == feedId) {
                    feedChannel = channel.asTextChannel().get();
                    break;
                }
            }
        }
    }

    private CompletableFuture<Void> getExistingInvites() {
        return serverAegis.getInvites().thenAccept(data -> {
            invites = data;
        });
    }

    private ArrayList<RichInvite> findInvLinksDifference(Collection<RichInvite> oldInvites) {
        // arraylist storing differences between new and old links
        ArrayList<RichInvite> diff = new ArrayList<RichInvite>();
        // iterate over old invites
        for(RichInvite old : oldInvites) {
            // cache invite link
            String oldCode = old.getCode();
            // flag we use to determine missing invite (had only one use left and got deleted)
            boolean found = false;
            for(RichInvite current : invites){
                // compare codes
                if(oldCode.equals(current.getCode())) {
                    found = true;
                    // compare times used counter
                    if(old.getUses() != current.getUses()) {
                        diff.add(current);
                    }
                    // corresponding invite found, breaking out of inner loop
                    break;
                }
            }
            // old invite not found in new list, adding to the differences list
            if(!found) {
                diff.add(old);
            }
        }
        return diff;
    }

    private void addShutdownHook() {
        try {
            Runtime.getRuntime().addShutdownHook(new Thread(){
                                                            public void run(){
                                                                System.out.println("Received interrupt signal, commencing shutting down procedure...");
                                                                
                                                                tasksThread.reason = InterruptReason.BOT_SHUTDOWN;
                                                                tasksThread.interrupt();

                                                                api.disconnect();

                                                                System.out.println("API disconnected.");
                                                                System.out.println("Shutting down...");
                                                            }
            });
        } catch(Exception e) {
            e.printStackTrace();
        }   
    }

    // interface for schedulable tasks (like periodically refreshing invites list)
    public interface PeriodicTask {
        // method overloaded by class implementing this interface
        // accepts InterruptReason enum, used when we want to use different features of the task
        // returns time until next awake
        public long exec(InterruptReason r);
    } 

    // class stores information about scheduled task
    class RunnableTask {
        // 
        PeriodicTask f;
        // UNIX timestamp of time the process is supposed to wake up next time
        long nextAwake;
        // default sleep length, in case obtained value is too high or gives error
        long sleepTimeDefault;

        RunnableTask(PeriodicTask r, long t) {   f = r;  sleepTimeDefault = t;   nextAwake = t + Instant.now().getEpochSecond(); }
    }

    // enum used to set reason why we interrupt task thread
    public enum InterruptReason {
        NONE,                                   // normal, periodic run of a task
        BOT_SHUTDOWN,                           // but shutting down, stop thread

                                                // TASK SPECIFIC FLAGS
        PULL_NEW_SERVER_INVITES,                // download new invites and find next task to awake to
        UPDATE_SLEEP_TIME_INVITES_REFRESHER;    // only find out next task to awake to (invites downloaded by other means)
    }

    // probably unnecessary but it was an interesting idea to play around with
    // run() method runs in a seperate thread and periodically executes tasks on its internal array
    // tasks are run either when their timer indicates it, or when we interrupt the thread with a proper flag set
    class TasksThread extends Thread {
        // next task to execute
        RunnableTask nextTask = null;
        // variable set before interrupting thread
        // used when we want to awake thread and run a given task before its scheduled execute time
        InterruptReason reason;
        // list of task the thread executes
        ArrayList<RunnableTask> tasks; 

        TasksThread() {
            nextTask = null;
            tasks = new ArrayList<RunnableTask>();
            reason = InterruptReason.NONE;
        }

        // adds new runnable task to the list
        // TODO: add new tasks while thread is running
        void registerTask(PeriodicTask r, long defaultSleep) {
            tasks.add(new RunnableTask(r, defaultSleep));
        }

        public void run() {
            // thread sleep duration and default value that we start from and look for tasks scheduled to execute earlier
            long nextAwakeDefault = 60L*60L*24L*14L;
            long nextAwake = nextAwakeDefault;

            // initial run of all tasks
            for(RunnableTask r : tasks) {
                long t = r.f.exec(InterruptReason.NONE);
                if(t >= 0) {
                    if(t > r.sleepTimeDefault)
                        t = r.sleepTimeDefault;
                    r.nextAwake = t + Instant.now().getEpochSecond();
                } else if(t == -2) {
                    t = r.sleepTimeDefault;
                    r.nextAwake = r.sleepTimeDefault + Instant.now().getEpochSecond();
                }

                if(t < nextAwake) {
                    nextAwake = t;
                    nextTask = r;
                }
            }

            while(true) {

                // put thread to sleep until next tasks is scheduled to execute
                System.out.println("Sleeping for: " + nextAwake + " seconds.");
                try {
                    Thread.sleep(nextAwake*1000);
                } catch(InterruptedException e) { }

                // bot shutting down, break out of the loop
                if(reason == InterruptReason.BOT_SHUTDOWN) {
                    break;
                }

                // current UNIX time
                // Instant.now().getEpochSecond();

                // run task we just interrupted sleep for
                if(reason != InterruptReason.NONE) {
                    // all tasks are ran, but only one recognizes the flag and executes its work code
                    for(RunnableTask r : tasks) {
                        long t = r.f.exec(reason);
                        if(t >= 0) {                        // set to refresh after invite expires
                            if(t > r.sleepTimeDefault)      // duration greater than default max sleep time
                                t = r.sleepTimeDefault;
                            r.nextAwake = Instant.now().getEpochSecond() + t;  
                                                            // set sleep time to returned value + current UNIX time
                        } else if (t == -2) {               // infinite sleep duration -> set to default sleep duration of the task
                            r.nextAwake = Instant.now().getEpochSecond() + r.sleepTimeDefault;
                        }
                    }
                    reason = InterruptReason.NONE;
                } else if(nextTask != null) {               // run next scheduled task after waking up
                                                            // same actions as above but only for single task
                    long t = nextTask.f.exec(reason);

                    if(t >= 0) {
                        if(t > nextTask.sleepTimeDefault)
                            t = nextTask.sleepTimeDefault;
                        nextTask.nextAwake = Instant.now().getEpochSecond() + t;
                    } else if (t == -2) {
                        nextTask.nextAwake = Instant.now().getEpochSecond() + nextTask.sleepTimeDefault;
                    }
                }

                // find when thread should wake up again to perform next task
                nextAwake = nextAwakeDefault + Instant.now().getEpochSecond();
                // loop over all tasks
                for(RunnableTask t : tasks) {
                    // and compare compare their nextAwake value
                    if(t.nextAwake < nextAwake) {
                        // task scheduled to execute sooner than previous found
                        nextAwake = t.nextAwake;
                        nextTask = t;
                    }
                }
                nextAwake -= Instant.now().getEpochSecond();
                // in case of some stupid errors in code
                if(nextAwake < 0)
                    nextAwake = 0;

            }
        }
    }

}
